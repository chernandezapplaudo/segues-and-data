//
//  ViewController.swift
//  PracticingNavigation
//
//  Created by APPLAUDO on 6/2/22.
//

import UIKit

class ViewController: UIViewController, ViewControllerDelegate {

    @IBOutlet weak var titleMsgLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func goToScreenA(_ sender: UIButton) {
        
        performSegue(withIdentifier: "go_to_screen_a", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go_to_screen_a"{
            if let controller = segue.destination as? ScreenAViewController {
              controller.titleText = "Hello from Home"
            }
        }
        if segue.identifier == "screen_b"{
            if let controller = segue.destination as? ScreenBViewController {
                controller.delegateBack = self
            }
        }
    }
    
    @IBAction func unwindScreen(segue: UIStoryboardSegue){
        
        if let controller = segue.source as? ScreenAViewController{
            if controller.textFormField.text != nil{
                titleMsgLabel.text = controller.textFormField.text
            }
        }
    }
    
    @IBAction func goToScreenB(_ sender: UIButton) {
        performSegue(withIdentifier: "screen_b", sender: nil)
    }
    
    func sendDataBack(data: String) {
        titleMsgLabel.text = data
    }
    
}

