//
//  ScreenBViewController.swift
//  PracticingNavigation
//
//  Created by APPLAUDO on 7/2/22.
//

import UIKit

protocol ViewControllerDelegate : NSObjectProtocol{
    func sendDataBack(data:String)
}

class ScreenBViewController: UIViewController {
    
    weak var delegateBack : ViewControllerDelegate?

    override func viewDidLoad() {
        if let delegate = delegateBack {
            delegate.sendDataBack(data: "hello from screen B!")
        }
        super.viewDidLoad()
    }
    
}
