//
//  ScreenAViewController.swift
//  PracticingNavigation
//
//  Created by APPLAUDO on 6/2/22.
//

import UIKit

class ScreenAViewController: UIViewController {

    /// this will be injected with segue
    var titleText: String!
    
    @IBOutlet weak var textFormField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = titleText
        // Do any additional setup after loading the view.
    }
    
}
